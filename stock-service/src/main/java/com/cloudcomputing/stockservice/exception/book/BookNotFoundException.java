package com.cloudcomputing.stockservice.exception.book;

public class BookNotFoundException extends Exception {

    public BookNotFoundException(String message) {
        super(message);
    }
}
