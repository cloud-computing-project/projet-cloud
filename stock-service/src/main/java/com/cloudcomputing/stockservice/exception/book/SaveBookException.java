package com.cloudcomputing.stockservice.exception.book;

public class SaveBookException extends Exception {

    public SaveBookException(String message) {
        super(message);
    }
}
