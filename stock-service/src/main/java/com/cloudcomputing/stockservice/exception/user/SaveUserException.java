package com.cloudcomputing.stockservice.exception.user;

public class SaveUserException extends Exception {

    public SaveUserException(String message) {
        super(message);
    }
}
