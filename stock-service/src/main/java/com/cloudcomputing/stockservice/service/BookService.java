package com.cloudcomputing.stockservice.service;

import com.cloudcomputing.stockservice.entity.Book;
import com.cloudcomputing.stockservice.exception.book.BookNotFoundException;
import com.cloudcomputing.stockservice.exception.book.SaveBookException;

import java.util.List;

public interface BookService {
    Book saveOne(Book book) throws SaveBookException;

    Integer getStock(String isbn) throws BookNotFoundException;

    void updateQuantity(Integer quantity, String isbn) throws SaveBookException;

    List<Book> findAll() throws Exception;

    void delete(String id) throws BookNotFoundException;
}
