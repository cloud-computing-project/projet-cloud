package com.cloudcomputing.stockservice.service.implementation;

import com.cloudcomputing.stockservice.entity.Book;
import com.cloudcomputing.stockservice.exception.book.BookNotFoundException;
import com.cloudcomputing.stockservice.exception.book.SaveBookException;
import com.cloudcomputing.stockservice.service.BookService;
import com.googlecode.objectify.ObjectifyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class BookServiceImpl implements BookService {
    @Override
    public Book saveOne(Book book) throws SaveBookException {
        try {
            final Book bookToSave = buildBook(book);

            ObjectifyService.ofy()
                    .save()
                    .entity(bookToSave)
                    .now();

            return bookToSave;
        } catch (SaveBookException ex) {
            throw new SaveBookException(ex.getMessage());
        }

    }

    @Override
    public Integer getStock(String isbn) throws BookNotFoundException {
        try {
            final Book book = ObjectifyService.ofy()
                    .load()
                    .type(Book.class)
                    .id(isbn)
                    .safe();

            return book.getQuantity();
        } catch (Exception ex) {
            final String errorMessage = String.format("Book with isbn %s not found !", isbn);
            throw new BookNotFoundException(errorMessage);
        }
    }

    @Override
    public void updateQuantity(Integer quantity, String isbn) throws SaveBookException {
        try {
            Book book = ObjectifyService.ofy()
                    .load()
                    .type(Book.class)
                    .id(isbn)
                    .safe();

            book.setQuantity(quantity);

            if (Objects.isNull(book.getQuantity())) {
                throw new SaveBookException("Quantity cannot be null.");
            }

            ObjectifyService.ofy()
                    .save()
                    .entity(book)
                    .now();
        } catch (SaveBookException ex) {
            throw new SaveBookException(ex.getMessage());
        }
    }

    private Book buildBook(Book book) throws SaveBookException {
        if (Objects.isNull(book.getTitle()) || Objects.isNull(book.getQuantity())) {
            throw new SaveBookException("Title or quantity cannot be null.");
        }

        final String uuid = generateRandomUUID();

        return Book.builder()
                .id(uuid)
                .isbn(uuid)
                .title(book.getTitle())
                .quantity(book.getQuantity())
                .build();
    }

    private String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }

    @Override
    public List<Book> findAll() {
        return ObjectifyService.ofy()
                .load()
                .type(Book.class)
                .list();
    }

    @Override
    public void delete(String id) throws BookNotFoundException {
        try {
            ObjectifyService.ofy()
                    .load()
                    .type(Book.class)
                    .id(id)
                    .safe();
        } catch (Exception ex) {
            final String errorMessage = String.format("Book with id %s not found !", id);
            throw new BookNotFoundException(errorMessage);
        }

        ObjectifyService.ofy()
                .delete()
                .type(Book.class)
                .id(id)
                .now();
    }
}
