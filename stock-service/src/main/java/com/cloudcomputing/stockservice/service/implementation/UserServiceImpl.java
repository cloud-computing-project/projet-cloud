package com.cloudcomputing.stockservice.service.implementation;

import com.cloudcomputing.stockservice.entity.Book;
import com.cloudcomputing.stockservice.entity.User;
import com.cloudcomputing.stockservice.exception.book.BookNotFoundException;
import com.cloudcomputing.stockservice.exception.user.SaveUserException;
import com.cloudcomputing.stockservice.exception.user.UserNotFoundException;
import com.cloudcomputing.stockservice.service.UserService;
import com.googlecode.objectify.ObjectifyService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User saveOne(User user) throws SaveUserException {
        try {
            final User userToSave = buildUser(user);

            ObjectifyService.ofy()
                    .save()
                    .entity(userToSave)
                    .now();
            return userToSave;
        } catch (SaveUserException ex) {
            throw new SaveUserException("Pseudo cannot be null");
        }
    }

    private User buildUser(User user) throws SaveUserException {
        if (Objects.isNull(user.getPseudo())) {
            throw new SaveUserException("Pseudo cannot be null");
        }

        final String uuid = generateRandomUUID();

        return User.builder()
                .id(uuid)
                .pseudo(user.getPseudo())
                .build();
    }

    private String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }

    @Override
    public List<User> findAll() {
        return ObjectifyService.ofy()
                .load()
                .type(User.class)
                .list();
    }

    @Override
    public void delete(String id) throws UserNotFoundException {
        try {
            ObjectifyService.ofy()
                    .load()
                    .type(User.class)
                    .id(id)
                    .safe();
        } catch (Exception ex) {
            final String errorMessage = String.format("User with id %s not found !", id);
            throw new UserNotFoundException(errorMessage);
        }

        ObjectifyService.ofy()
                .delete()
                .type(User.class)
                .id(id)
                .now();
    }
}
