package com.cloudcomputing.stockservice.service;

import com.cloudcomputing.stockservice.entity.User;
import com.cloudcomputing.stockservice.exception.user.SaveUserException;
import com.cloudcomputing.stockservice.exception.user.UserNotFoundException;

import java.util.List;

public interface UserService {
    User saveOne(User user) throws SaveUserException;

    List<User> findAll() throws Exception;

    void delete(String id) throws UserNotFoundException;
}
