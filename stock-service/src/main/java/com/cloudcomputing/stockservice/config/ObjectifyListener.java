package com.cloudcomputing.stockservice.config;

import com.cloudcomputing.stockservice.entity.User;
import com.cloudcomputing.stockservice.entity.Book;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ObjectifyListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ObjectifyService.init();
        ObjectifyService.register(Book.class);
        ObjectifyService.register(User.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) { }
}
