package com.cloudcomputing.stockservice.controller;

import com.cloudcomputing.stockservice.entity.User;
import com.cloudcomputing.stockservice.exception.book.BookNotFoundException;
import com.cloudcomputing.stockservice.exception.user.SaveUserException;
import com.cloudcomputing.stockservice.exception.user.UserNotFoundException;
import com.cloudcomputing.stockservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/v1.0/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<?> findAll() {
        try {
            final List<User> users = userService.findAll();

            return ResponseEntity.ok(users);
        } catch (Exception ex) {
            return ResponseEntity.status(NOT_FOUND).body("No users found.");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable String id) {
        try {
            userService.delete(id);

            return ResponseEntity.status(NO_CONTENT).body("User deleted with success !");
        } catch (UserNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<?> saveOne(@RequestBody User user) {
        try {
            final User userSaved = userService.saveOne(user);

            return ResponseEntity.status(CREATED).body(userSaved);
        } catch(SaveUserException ex) {
            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(ex.getMessage());
        }
    }
}
