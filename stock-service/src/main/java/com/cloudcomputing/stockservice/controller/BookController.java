package com.cloudcomputing.stockservice.controller;

import com.cloudcomputing.stockservice.entity.Book;
import com.cloudcomputing.stockservice.exception.book.BookNotFoundException;
import com.cloudcomputing.stockservice.exception.book.SaveBookException;
import com.cloudcomputing.stockservice.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/v1.0/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping
    public ResponseEntity<?> findAll() {
        try {
            final List<Book> books = bookService.findAll();

            return ResponseEntity.ok(books);
        } catch (Exception ex) {
            return ResponseEntity.status(NOT_FOUND).body("No books found");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable String id) {
        try {
            bookService.delete(id);

            return ResponseEntity.status(NO_CONTENT).body("Book deleted with success !");
        } catch (BookNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }
    }

    @GetMapping("/available-stock/{isbn}/{username}")
    public ResponseEntity<?> getAvailableStockBook(@PathVariable String isbn,
                                                   @PathVariable String username) {
        try {
            final Integer quantity = bookService.getStock(isbn);

            final String responseMessage = quantity == 0 ? "Unavailable" : "Available";

            return ResponseEntity.ok(responseMessage);
        } catch(BookNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }

    }

    @GetMapping("/quantity/{isbn}/{username}")
    public ResponseEntity<?> getQuantityBook(@PathVariable String isbn, @PathVariable String username) {
        try {
            final Integer stockOfBook = bookService.getStock(isbn);

            return ResponseEntity.ok(stockOfBook);
        } catch(BookNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }

    }

    @PostMapping("/{username}")
    public ResponseEntity<?> saveOne(@RequestBody Book book, @PathVariable String username) {
        try {
            final Book bookSaved = bookService.saveOne(book);

            return ResponseEntity.status(CREATED).body(bookSaved);
        } catch(SaveBookException ex) {
                return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(ex.getMessage());
        }
    }

    @PutMapping("/{quantity}/{isbn}/{username}")
    public ResponseEntity<?> updateQuantity(@PathVariable Integer quantity,
                                            @PathVariable String isbn,
                                            @PathVariable String username) {
        try {
            bookService.updateQuantity(quantity, isbn);

            return ResponseEntity.noContent().build();
        } catch(SaveBookException ex) {
            return ResponseEntity.status(UNPROCESSABLE_ENTITY).body(ex.getMessage());
        }
    }
}
