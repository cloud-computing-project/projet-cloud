package com.cloudcomputing.shoppingservice.service;

import com.cloudcomputing.shoppingservice.exception.BookNotFoundException;
import com.cloudcomputing.shoppingservice.models.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.cloudcomputing.shoppingservice.config.WebConfig.STOCK_SERVER_URL;
import static com.cloudcomputing.shoppingservice.config.WebConfig.WHOLESALER_SERVER_URL;


@Service
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    private final RestTemplate restTemplate;

    @Override
    public String getAvailableStockBook(String isbn, String username) throws BookNotFoundException {
        try {
            return restTemplate.getForObject(
                    String.format("%s/available-stock/%s/%s", STOCK_SERVER_URL, isbn, username), String.class);
        } catch (Exception ex) {
            throw new BookNotFoundException(String.format("Book with isbn %s not found.", isbn));
        }
    }

    @Override
    public Integer getQuantityBook(String isbn, String username) throws BookNotFoundException {
        try {
            return restTemplate.getForObject(
                    String.format("%s/quantity/%s/%s", STOCK_SERVER_URL, isbn, username), Integer.class);
        } catch (Exception ex) {
            throw new BookNotFoundException(
                    String.format(
                            "Unable to obtain the desired quantity of books for the book with isbn %s",
                            isbn));
        }
    }

    @Override
    public Integer supplyBooks(String username) {
        return restTemplate.getForObject(
                String.format("%s/%s", WHOLESALER_SERVER_URL, username), Integer.class);
    }

    @Override
    public void updateQuantity(Book book, String username) throws BookNotFoundException {
        try {
            restTemplate.put(String.format("%s/%d/%s/%s", STOCK_SERVER_URL, book.getQuantity(), book.getIsbn(), username), book);
        } catch (Exception ex) {
            throw new BookNotFoundException(
                    String.format(
                            "Unable to obtain the desired quantity of books for the book with isbn %s",
                            book.getIsbn()));
        }
    }

    @Override
    public String buy(Integer quantity, String isbn, String username) throws BookNotFoundException {
        try {

            Integer quantityBookStock = getQuantityBook(isbn, username);

            while (quantity > quantityBookStock) {
                quantityBookStock += supplyBooks(username);
            }

            quantityBookStock -= quantity;

            final Book book = Book.builder()
                    .id(isbn)
                    .isbn(isbn)
                    .quantity(quantityBookStock)
                    .build();

            updateQuantity(book, username);

            return "Order ready";
        } catch (Exception ex) {
            throw new BookNotFoundException(String.format("Unable to obtain the desired quantity of books for the book with isbn %s", isbn));
        }
    }
}
