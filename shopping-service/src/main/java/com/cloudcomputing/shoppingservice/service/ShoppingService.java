package com.cloudcomputing.shoppingservice.service;

import com.cloudcomputing.shoppingservice.exception.BookNotFoundException;
import com.cloudcomputing.shoppingservice.models.Book;

public interface ShoppingService {
    String getAvailableStockBook(String isbn, String username) throws BookNotFoundException;
    Integer getQuantityBook(String isbn, String username) throws BookNotFoundException;
    Integer supplyBooks(String username);
    void updateQuantity(Book book, String username) throws BookNotFoundException;
    String buy(Integer quantity, String isbn, String username) throws BookNotFoundException;
}
