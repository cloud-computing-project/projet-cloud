package com.cloudcomputing.shoppingservice.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WebConfig {

    public final static String STOCK_SERVER_URL = "https://stock-service-l3-project.uc.r.appspot.com/api/v1.0/books";
    public final static String WHOLESALER_SERVER_URL = "https://wholesalerservice.herokuapp.com/api/v1.0/whole-saler";
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }
}
