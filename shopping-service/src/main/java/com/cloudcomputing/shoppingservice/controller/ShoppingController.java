package com.cloudcomputing.shoppingservice.controller;

import com.cloudcomputing.shoppingservice.exception.BookNotFoundException;
import com.cloudcomputing.shoppingservice.service.ShoppingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api/v1.0/shopping")
@RequiredArgsConstructor
public class ShoppingController {

    private final ShoppingService shoppingService;

    @GetMapping("/{isbn}/{username}")
    public ResponseEntity<?> getStock(@PathVariable String isbn, @PathVariable String username) {
        try {
            final String availableStock =
                    shoppingService.getAvailableStockBook(isbn, username);

            return ResponseEntity.ok(availableStock);
        } catch(BookNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }
    }

    @GetMapping("/buy/{isbn}/{quantity}/{username}")
    public ResponseEntity<?> buyBooks(@PathVariable String isbn,
                                      @PathVariable Integer quantity,
                                      @PathVariable String username) {
        try {
            final String responseMessage = shoppingService.buy(quantity, isbn, username);

            return ResponseEntity.ok(responseMessage);
        } catch(BookNotFoundException ex) {
            return ResponseEntity.status(NOT_FOUND).body(ex.getMessage());
        }
    }
}
