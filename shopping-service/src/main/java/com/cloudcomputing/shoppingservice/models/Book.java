package com.cloudcomputing.shoppingservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Book {
    private String id;
    private String isbn;
    private String title;
    private Integer quantity;
}
