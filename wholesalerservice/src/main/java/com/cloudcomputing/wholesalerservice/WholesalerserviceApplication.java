package com.cloudcomputing.wholesalerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WholesalerserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WholesalerserviceApplication.class, args);
    }

}
