package com.cloudcomputing.wholesalerservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@CrossOrigin
@RestController
@RequestMapping("/api/v1.0/whole-saler")
public class WholesealerController {

    @GetMapping("/{username}")
    public ResponseEntity<?> getWholeSealer(@PathVariable String username) {
        try {
            return ResponseEntity.ok(5);
        } catch(Exception ex) {
            final String errorMessage = "An error occurred in server.";
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }
}
